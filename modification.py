import re
from typing import Tuple

from aiohttp import ClientSession
from aiohttp.web_request import BaseRequest
from aiohttp.web_response import Response
from lxml import html
from lxml.html import HtmlElement


class Modification(object):
    allowed_tags = ["a", "abbr", "b", "blockquote", "script",
                    "br", "div", "em", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "i", "label", "li", "p", "pre",
                    "small", "span", "strong", "ul"]  # Теги в которых меняем

    def __init__(self, url: str = '', symbol: str = '™', count: int = 6, port: str = '', host: str = ''):
        self.url = url
        self.symbol = symbol
        self.host = host
        self.port = port
        self.pattern = r'(?<!\w)\w{{{count_words}}}(?=[^\w]|$)'.format(count_words=count)

    async def handler(self, request: BaseRequest) -> Response:
        """
        Основной обработчик
        TODO исключения
        :param request:
        :return:
        """
        text = 'Ok'
        is_text, content = await self.get_text(request.path)

        if is_text:
            body = html.fromstring(content)
            body.rewrite_links(self.replace_link)
            text = await self.replace_tm(body)

        response = {'text': text,
                    'content_type': 'text/html'}
        return Response(**response)

    async def get_text(self, path: str) -> Tuple[bool, str]:
        """
        Получаем данные с сайта, проверяем текст это или нет
        :return: проверка на текст, текст
        """
        if path.startswith('/'):
            path = path[1:]
        async with ClientSession() as session:
            async with session.get(self.url + path) as response:
                if 'text/html' in response.headers['Content-Type']:
                    return True, await response.text()
                return False, await response.read()

    async def replace_tm(self, body: HtmlElement) -> str:
        """
        Обработчик замены.
        Ходим по выбранным тегам и меняем там текст
        :param body: HtmlElement текст html
        :return:
        """
        for item in body.iter(self.allowed_tags):
            if item.tag == 'script':
                if 'src' in item.attrib and item.attrib.get('src').startswith('/'):
                    item.attrib.update({'src': self.url[0:-1] + item.attrib['src']})
                continue
            if item.tail:
                item.tail = await self.add_tm(item.tail)
            if item.text:
                item.text = await self.add_tm(item.text)

        return html.tostring(body, encoding='unicode')

    async def add_tm(self, text: str) -> str:
        """
        Добавялем любой символ в тексте
        :param text: входной любой тект
        :return:
        """
        text = re.sub(self.pattern, (lambda m: m.group() + self.symbol), text).replace('&plus;', '+')
        return text

    def replace_link(self, text: str) -> str:
        """
        Меняем ссылки сайта, на свои
        :param text:
        :return:
        """
        return text.replace(self.url, 'http://{}:{}/'.format(self.host, self.port))
