import logging

logger = logging.getLogger(__name__)
formatter = logging.Formatter('[%(levelname)s][%(asctime)s]: %(message)s')

handler = logging.StreamHandler()
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.propagate = 0
logging.info('Starting logger for...')
logger.setLevel('DEBUG')
