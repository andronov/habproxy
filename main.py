import argparse
import asyncio
from aiohttp import web
from modification import Modification
from settings import logger

parser = argparse.ArgumentParser(description='Аргументы для изменения текста при помощи символа')
parser.add_argument('--url', metavar='-u', type=str, nargs='?', default='https://habrahabr.ru/',
                    help='Сайт на который переходим')
parser.add_argument('--host', metavar='-h', type=str, nargs='?', default='127.0.0.1',
                    help='Адрес на котором запускаем')
parser.add_argument('--port', metavar='-p', type=str, nargs='?', default=8888,
                    help='Порт на котором запускаем')
parser.add_argument('--count', metavar='-c', type=str, nargs='?', default=6,
                    help='Кол. символом после которого его добавляем')
parser.add_argument('--symbol', metavar='-s', type=str, nargs='?', default='™', help='Символ который добавляем')


async def main():
    args = parser.parse_args()
    modificator = Modification(**vars(args))

    server = web.Server(modificator.handler)
    await loop.create_server(server, args.host, args.port)
    logger.info('"======= Serving on http://%s:%i/ ======', args.host, args.port)

    # pause here for very long time by serving HTTP requests and
    # waiting for keyboard interruption
    await asyncio.sleep(100*3600)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        pass
    loop.close()
